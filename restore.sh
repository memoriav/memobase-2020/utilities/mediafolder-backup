echo -n "Checking if folder $REMOTE_FOLDER on remote host exists..."
ssh -i /etc/ssh-key/id_rsa -o 'StrictHostKeyChecking=no' $REMOTE_USER@$REMOTE_HOST test -e $REMOTE_FOLDER &>/dev/null && \
echo " OK"
echo -n "Restore media files..."
echo rsync -rvptgou -e 'ssh -i /etc/ssh-key/id_rsa -o "StrictHostKeyChecking=no"' $REMOTE_USER@$REMOTE_HOST:$REMOTE_FOLDER /media/
rsync -rvptgou -e 'ssh -i /etc/ssh-key/id_rsa -o "StrictHostKeyChecking=no"' $REMOTE_USER@$REMOTE_HOST:$REMOTE_FOLDER /media/ && \
echo " OK"
