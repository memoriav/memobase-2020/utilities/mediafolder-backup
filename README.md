# Mediafolder Backup (and Restore)

This repository provides Docker Images for performing backups and restores of
the Memobase media folder to / from an external share.

In order to run the synchronisation process, you have to generate an SSH key
pair and propagate the _public_ key to the remote host. Furthermore, you must
save the _private_ key as a Kubernetes secret where it will be accessed by
the process.

1. `ssh-keygen -t ed25519 -C "your_email@example.com -f path/to/key"`
2. `ssh-copy-id -i path/to/key.pub username@remoteHost`
3. `kubectl create secret generic adm-media-folder-backup-secrets --from-file=id_rsa=path/to/key --from-file=id_rsa.pub=path/to/key.pub`
## Deployment

As the *backup job* should run on a regular basis, it is recommended to deploy it as a Kubernets `CronJob`. See the [Helm Charts Registry](https://gitlab.switch.ch/memoriav/memobase-2020/configurations/helm-charts-registry/-/tree/main/media-folder-backup) for respective Helm charts.

Because the *restore job* should only be started when an actual restore is required, the Helm charts can directly be found in this repository: [`/restore-helm-charts`](./restore-helm-charts/).

Deploy it with the following command:

```sh
ENV=<your_env>
helm install -f restore-helm-charts/values/${ENV}.yaml adm-media-folder-restore-${ENV} ./restore-helm-charts
```

...and please clean up afterwards:

```sh
helm delete adm-media-folder-restore-${ENV} ./restore-helm-charts
```
