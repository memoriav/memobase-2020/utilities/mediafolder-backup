echo -n "Checking if folder $REMOTE_FOLDER on remote host exists..."
ssh -i /etc/ssh-key/id_rsa -o 'StrictHostKeyChecking=no' $REMOTE_USER@$REMOTE_HOST test -e $REMOTE_FOLDER &>/dev/null && \
echo " OK"
echo -n "Backup media files..."
rsync -rvptgou -e 'ssh -i /etc/ssh-key/id_rsa -o "StrictHostKeyChecking=no"' --del /media/ $REMOTE_USER@$REMOTE_HOST:$REMOTE_FOLDER && \
echo " OK"
